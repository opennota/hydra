hydra [![License](http://img.shields.io/:license-gpl3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0.html) [![Pipeline status](https://gitlab.com/opennota/hydra/badges/master/pipeline.svg)](https://gitlab.com/opennota/hydra/commits/master)
=====

Hydra is a penetration testing tool exclusively focused on dictionary-attacking web-based login forms.

## Installation

    go get -u gitlab.com/opennota/hydra

## Usage

    hydra -L logins.txt -P passwords.txt http://127.0.0.1/login "user=^USER^&pass=^PASS^" "invalid password"

Run `hydra -help` for additional options.
